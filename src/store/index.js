import { createStore, applyMiddleware } from "redux";
import reduxPromise from "redux-promise";
import { persistStore } from "redux-persist";
import { persistReducer } from "redux-persist";
import storage from "redux-persist/lib/storage";
import { composeWithDevTools } from "redux-devtools-extension";

import rootReducer from "./rootReducer";

const persistConfig = {
    key: "mui5",
    storage: storage,
    blacklist: [],
};

const persistedReducer = persistReducer(persistConfig, rootReducer);

const store = createStore(
    persistedReducer,
    composeWithDevTools(applyMiddleware(reduxPromise))
);

const persiStore = persistStore(store);

export { store, persiStore };
