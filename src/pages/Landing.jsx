import React from "react";
import { Badge, Box, Container, Typography } from "@mui/material";
export default function Landing() {
    return (
        <Box sx={{ background: "#dedede", width: "100%", height: "100vh" }} component="section">
            <Container >
                <Box
                    sx={{
                        position: "absolute",
                        top: "40%",
                        left: "50%",
                        transform: "translate(-50%, -50%)",
                        textAlign: "center",
                    }}
                >
                    <Typography variant="h2" color="secondary">Welcome to React - Redux - MUI 5</Typography>

                    <p>Let's start building amazing things !</p>
                </Box>
            </Container>
        </Box>
    );
}
